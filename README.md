# codeberg-images

Collection of codeberg images to use to redirect users from other services.
All images are generated using https://get-it-on.codeberg.org/

### Example usage for a git project being mirrored on GitHub
<table>
    <td>
        <a href="https://codeberg.org/abdul2906/codeberg-images/">
            <img alt="This project is hosted on codeberg" src="https://codeberg.org/abdul2906/codeberg-images/raw/branch/main/hosted_on_codeberg_blue.png">
        </a>
    </td>
    <td>
        This project is hosted on codeberg. No issues or merge requests will be reviewed on GitHub. This project is merely mirrored for traffic. Head over to https://codeberg.org/abdul2906/codeberg-images/ to contribute.
    </td>
</table>